const DecisionTreeHelper = require('../utils/decision_tree_helper');
const expect = require('chai').expect;

it('works when image attributes are provided correctly', (done) => {
    const attributes = [
        {
            attribute: 'gender',
            question: 'Are you looking for men or women',
            enabled: true,
            image_attribute: 'image_url',
            values: [],
        },
        {
            attribute: 'fit_type',
            enabled: false,
            image_attribute: 'image_url',
            values: [],
        },
        {
            attribute: 'image_url',
            enabled: false,
            values: [],
        },
    ];
    expect(() => DecisionTreeHelper.validateAttributes(attributes)).to.not.throw();
    done();
});

it('throws when image attributes are not provided correctly', (done) => {
    const attributes = [
        {
            attribute: 'gender',
            question: 'Are you looking for men or women',
            enabled: true,
            image_attribute: 'image_url',
            values: [],
        },
        {
            attribute: 'fit_type',
            enabled: false,
            image_attribute: 'image_url',
            values: [],
        },
    ];
    expect(() => DecisionTreeHelper.validateAttributes(attributes)).to.throw(TypeError, /is not one of the attributes/);
    done();
});
