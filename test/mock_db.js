var mongoose = require('mongoose');
var Mockgoose = require('mockgoose').Mockgoose;
var mockgoose = new Mockgoose(mongoose);
const uuid_v1 = require('uuid/v1');
var assert = require('chai').assert;
var expect = require('chai').expect;

before(function(done){
    // This is needed as per the author of mockgoose.
    // See https://github.com/Mockgoose/Mockgoose/issues/14
    this.timeout(60000)
    mockgoose.prepareStorage().then(function() {
	    // mongoose connection
	    mongoose.connect('mongodb://example.com/TestingDB', function(err) {
            Intent = require('../../common/models/intent');
            done(err);
        });
    });
})

beforeEach(function(done){
    mockgoose.helper.reset()
    bot_id = uuid_v1();
    Intent.create([{_id: bot_id+"_intent_1", name: "search-intent"},
                  {_id: bot_id+"_intent_2", name: "greeting-intent"},
                  {_id: bot_id+"_intent_100", name: "some_other-intent"}], function(err, intent_docs){
        if (err) {
            console.log("Could not create an intent: ", err);
            return done(err);
        }
        intent_names_created = intent_docs.map(function(i) {return i.name});
        console.log("Created intents  ", intent_names_created);
        done();
    })
})


it ('should find the intents', function(done){
    Intent.getByBotId(bot_id, function(err, intent_docs) {
        if(err) {
            console.log("Could not find intents for bot " + bot_id);
            return done(err);
        } else {
            n_intents = intent_docs.length;
            assert.equal(n_intents, 3, "Found " + n_intents + " intents for bot " + bot_id);
            found_intent_names = intent_docs.map(function(i){return i.name});
            expect(found_intent_names).to.have.members(intent_names_created);
            return done(null);
        }
    })
})

it ('should throw an error bot mismatch error', function(done) {
    const intent_id = uuid_v1();
	Intent.getBotIntent(bot_id, intent_id, function(err, intent_doc){
	    const expected_err = `Intent ${intent_id} is not a member of bot ${bot_id}`;
	    expect(err).to.equal(expected_err);
	    return done(null);
	});
})

it ('should return an intent that belongs to a bot', function(done) {
    const intent_id = bot_id + "_intent_100";
	Intent.getBotIntent(bot_id, intent_id, function(err, intent_doc) {
	    expect(err).to.be.null;
	    expect(intent_doc.name).to.equal('some_other-intent');
	    return done(null);
	});
})
