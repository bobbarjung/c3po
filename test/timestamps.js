var AnalyticsHelper = require('../utils/analytics_helper')
var moment = require('moment')
var assert = require('chai').assert;
var expect = require('chai').expect;

it ('work without timezone', function(done) {
    startTs = moment('2017-04-01T10:00:00.000Z');
    endTs = moment('2017-04-01T20:00:00.000Z');
    result = AnalyticsHelper.getTimeStamps({start_time: startTs.format(), end_time: endTs.format()});
    expect(result).to.have.property('start_ts').and.equal(1491040800000);
    expect(result).to.have.property('end_ts').and.equal(1491076800000);
    expect(result).to.have.property('start_day').and.equal('2017-04-01')
    expect(result).to.have.property('start_month').and.equal('2017-04')
    expect(result).to.have.property('utc_offset').and.equal(0);
    done();
})

it ('works with timezone', function(done) {
    startTs = moment('2017-04-01T06:00:00.000Z');
    endTs = moment('2017-04-01T20:00:00.000Z');
    result = AnalyticsHelper.getTimeStamps({start_time: startTs.format(), end_time: endTs.format(), utc_offset: 420});
    expect(result).to.have.property('start_ts').and.equal(1491026400000);
    expect(result).to.have.property('end_ts').and.equal(1491076800000);
    expect(result).to.have.property('start_day').and.equal('2017-03-31');
    expect(result).to.have.property('start_month').and.equal('2017-03');
    expect(result).to.have.property('utc_offset').and.equal(420);
    done();
})

it ('with timezone and no start_time', function(done) {
    endTs = moment('2017-04-01T20:00:00.000Z');
    result = AnalyticsHelper.getTimeStamps({end_time: endTs.format(), utc_offset: 420});
    expect(result).to.have.property('start_ts').and.equal(1491030000000);
    expect(result).to.have.property('end_ts').and.equal(1491076800000);
    expect(result).to.have.property('utc_offset').and.equal(420);
    done();
})

it ('with timezone and no start_time and no end_time', function(done) {
    result = AnalyticsHelper.getTimeStamps({});
    expect(result).to.have.property('start_ts');
    expect(result).to.have.property('end_ts');
    done();
})

it ('should throw when start time > end time', function(done) {
    endTs = moment('2017-04-01T10:00:00.000Z');
    startTs = moment('2017-04-01T20:00:00.000Z');
    expect(() => AnalyticsHelper.getTimeStamps({start_time: startTs, end_time: endTs})).to.throw(
        'start time is greater than end time'
    );
    done();
})