var assert = require('assert');

// TODO: add real tests: https://rupertlabs.atlassian.net/browse/RL-219
describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal(-1, [1,2,3].indexOf(4));
    });
  });
});
