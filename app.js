const fs = require('fs');
const express = require('express');
const bots = require('./routes/bots');
const analytics = require('./routes/analytics');
const mongoose = require('mongoose');
const constants = require('../common/constants');
const logger = require('../common/utils/logger');
const bodyParser = require('body-parser');

const app = express();

app.get('/status', (req, res) => {
    const ret = {
        status: 'up',
        database: mongoose.connection.readyState === 1 ? 'connected' : 'Not connected',
    };
    return res.status(constants.OK).send(ret);
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/bots', bots);
app.use('/analytics', analytics);
app.post('/', (req, res) => res.status(200).send('OK'));

mongoose.connect(process.env.MONGODB_URI,
    {
        server: {
            poolSize: 5,
            ssl: true,
            sslValidate: true,
            sslCA: [fs.readFileSync(process.env.MONGODB_SSL_CA_FILE)],
        },
    });

mongoose.connection.on('error', () => { logger.info('MongoDB Connection Error.'); process.exit(1); });
if (process.env.MODE === 'development') {
    mongoose.set('debug', true);
}

const server = app.listen(3000, () => {
    logger.info('App is starting and listening on port: 3000');
});

module.exports = server;

