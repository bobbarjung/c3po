var pg = require('pg')
var logger = require('../../common/utils/logger');
var moment = require('moment');
var async = require('async');
var Intent = require('../../common/models/intent')

var client = {
  host: process.env.REDSHIFT_DB_HOST,
  port: process.env.REDSHIFT_DB_PORT,
  database: process.env.REDSHIFT_DB_NAME,
  user: process.env.REDSHIFT_USER_NAME,
  password: process.env.REDSHIFT_USER_PASSWORD,
  max: 10, // Max number of clients in the pool
  idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
};

var pg_pool = new pg.Pool(client);

var AnalyticsHelper = module.exports;

const REDSHIFT_DB_TABLE = 'rupert_bot_events';

// Get timestamps from req.query
// query will contain
//    start_time in iso8601 string
//    end_time in iso8601 string.
//    utc_offset in minutes - this will be considered for startOfDay and endOfDay calculations.
// returns start_ts and end_ts - which is the timestamp in miliseconds since the epoch.
AnalyticsHelper.getTimeStamps = function(query) {
    var start_time_param = (query || {}).start_time;
    var end_time_param = (query || {}).end_time || moment();
    // If a time zone offset is specified it will be in hours, compute it in milliseconds.\
    // Defaults to UTC, which is an offset of 0.
    var utc_offset = parseInt((query || {}).utc_offset || 0);
    // moment(undefined) defaults to now, which is great here
    var end_ts
    end_ts = moment.utc(end_time_param).toDate().getTime(); // Default to now.

    var start_ts, start_day;
    if (start_time_param) {
        start_ts = moment(start_time_param).toDate().getTime();
        start_day = moment.utc(start_time_param).subtract(utc_offset, 'minutes').startOf('day')
    } else  {
        // If start_ts_param is not defined, then go to start of day from end_time_param
        start_day = moment.utc(end_time_param).subtract(utc_offset, 'minutes').startOf('day')
        start_ts = start_day.add(utc_offset, 'minutes').toDate().getTime();
    }
    if (start_ts > end_ts) {
        throw ('start time is greater than end time');
    }
    return {
        start_ts: start_ts,
        end_ts: end_ts,
        start_day: start_day.format().substr(0, 10),
        start_month: start_day.format().substr(0, 7),
        utc_offset: utc_offset
    }
}

AnalyticsHelper.getChatMessages = function(req, res, next) {
    pg_pool.connect(function(err, client, client_done) {
        if (err) {
            logger.error("Could not connect to redshift. Reason: ", err);
            return res.status(500).send("Could not connecto to redshift: ", err);
        }
        try {
            ts = AnalyticsHelper.getTimeStamps(req.query);
        } catch (err) {
            logger.error("Could not get or validate timestamp. ", err);
            return res.status(400).send(err);
        }

        var query = "SELECT message_text,intent_id,intents,expanded_intents FROM " +
                    REDSHIFT_DB_TABLE +
                    " WHERE bot_id = " + "'" + req.params.bot_id + "'" +
                    (req.query.intent_id ? " AND intent_id = " + "'" + req.query.intent_id + "'" : "") +
                    " AND created_at >= " + ts.start_ts + " AND created_at <= " + ts.end_ts;
        logger.info("Executing query ", query);
        client.query(query, function(err, data) {
            client_done(null);
            if (err) {
                logger.error("Data warehouse returned an error: ", err);
                return res.status(500).send("Could not retrieve data from warehouse: ", err);
            }
            logger.info("Found ", data.rows.length, " records for query. Sending it to api caller.");
            return res.status(200).send(data.rows);
        })
    })
}

// Handler for GET /analytics/users API.
//
AnalyticsHelper.getUserInfo = function(req, res, next) {

    var time_obj;
    try {
        time_obj = AnalyticsHelper.getTimeStamps(req.query);
    } catch (err) {
        logger.error("Could not get or validate timestamp. ", err)
        return res.status(400).send(err);
    }
    // Start from start of the first day.
    var start_day = moment.utc(time_obj.start_day);
    var start_month = moment.utc(time_obj.start_month);
    var start_day_ts = moment.utc(time_obj.start_day).add(time_obj.utc_offset, 'minutes').toDate().getTime();
    var start_month_ts = moment.utc(time_obj.start_month).add(time_obj.utc_offset, 'minutes').toDate().getTime();

    var extract_data_fn = function(data, period) {
        result = {};
        if (period == 'total') {
            return data.rows[0]['total']
        }
        moment_offset = {
            daily: 10,
            monthly: 7
        }[period];
        data.rows.forEach(function(row){
            key = moment(row['created_interval']).format().substr(0, moment_offset);
            result[key] = row['count'];
        });
        return result;
    }
    tz_offset_msecs = time_obj.utc_offset * 60 * 1000;
    var query_template = "SELECT COUNT(distinct user_id) as count, " +
                         "DATE_TRUNC('period_to_replace', TIMESTAMP 'epoch' + ((created_at - " + tz_offset_msecs + ")/ 1000) * INTERVAL '1 second' ) AS created_interval" +
                         " FROM " + REDSHIFT_DB_TABLE +
                         " WHERE bot_id = " + "'" + req.params.bot_id + "'" +
                         " AND created_at BETWEEN start_timestamp_to_replace AND " + time_obj.end_ts +
                         " GROUP BY created_interval ORDER BY created_interval";
    var daily_query = query_template.replace('period_to_replace', 'day').replace('start_timestamp_to_replace', start_day_ts);
    var monthly_query = query_template.replace('period_to_replace', 'month').replace('start_timestamp_to_replace', start_month_ts);
    var total_query = "SELECT COUNT(distinct user_id) as total" +
                       " FROM " + REDSHIFT_DB_TABLE +
                       " WHERE bot_id = " + "'" + req.params.bot_id + "'" +
                       " AND created_at BETWEEN " + time_obj.start_ts + " AND " + time_obj.end_ts;


    makeParallelQueriesForDifferentTimePeriods(
        {
            daily: {
                query_string: daily_query,
                extract_data_fn: extract_data_fn
            },
            monthly: {
                query_string: monthly_query,
                extract_data_fn: extract_data_fn
            },
            total: {
                query_string: total_query,
                extract_data_fn: extract_data_fn
            }
        },
        function(err, results) {
            if (err) {
                logger.error("Error during one of the queries. Reason: ", err)
                return res.status(500).send(err);
            } else {
                res.header("Access-Control-Allow-Origin", "*");
                logger.info("Sending users analytics info to api caller")
                return res.status(200).send(results);
            }
        }
    )
}

// Handler for GET /analytics/intents function
AnalyticsHelper.getIntentInfo = function(req, res, next) {
    var time_obj;
    try {
        time_obj = AnalyticsHelper.getTimeStamps(req.query);
    } catch (err) {
        logger.error("Could not get or validate timestamp. ", err);
        return res.status(400).send(err);
    }
    var start_day_ts = moment.utc(time_obj.start_day).add(time_obj.utc_offset, 'minutes').toDate().getTime();
    var start_month_ts = moment.utc(time_obj.start_month).add(time_obj.utc_offset, 'minutes').toDate().getTime();
    logger.info("start_day_ts = ", start_day_ts, " start_month_ts = ", start_month_ts);
    var tz_offset_msecs = time_obj.utc_offset * 60 * 1000;
    var template_string = "SELECT COUNT(DISTINCT id) as count, " +
                          "DATE_TRUNC('period_to_replace', TIMESTAMP 'epoch' + ((created_at - " + tz_offset_msecs + ")/ 1000) * INTERVAL '1 second' ) AS created_interval, " +
                          "intent_id " +
                          "FROM " + REDSHIFT_DB_TABLE + " " +
                          "WHERE bot_id = '" + req.params.bot_id + "' " +
                          "AND created_at BETWEEN start_timestamp_to_replace" + " AND " + time_obj.end_ts + " " +
                          "GROUP BY intent_id, created_interval ORDER BY created_interval;"
    var daily_string = template_string.replace('period_to_replace','day').replace('start_timestamp_to_replace', start_day_ts);
    var monthly_string = template_string.replace('period_to_replace','month').replace('start_timestamp_to_replace', start_month_ts);

    var extract_data_fn = function(data, period) {
        result = {}
        moment_offset = {
            daily: 10,
            monthly: 7
        }[period]
        data.rows.forEach(function(row){
            if (!row['intent_id']) {
                return;
            }
            var key = moment(row['created_interval']).format().substr(0, moment_offset)
            if (!result[key]) {
                result[key] = {};
            }
            result[key][row['intent_id']] = row['count'];
        })
        return result;
    }

    makeParallelQueriesForDifferentTimePeriods(
        {
            daily: {
                query_string: daily_string,
                extract_data_fn: extract_data_fn
            },
            monthly: {
                query_string: monthly_string,
                extract_data_fn: extract_data_fn
            }
        },
        function(err, results) {
            if (err) {
                logger.error("Error during one of the queries. Reason: ", err);
                return res.status(500).send(err);
            } else {
                res.header("Access-Control-Allow-Origin", "*")
                logger.info("sending intents info to api caller")
                return res.status(200).send(results);
            }
        }
    );
}

AnalyticsHelper.getUniqueUsersForIntents = function(req, res, next) {
    var time_obj;
    try {
        time_obj = AnalyticsHelper.getTimeStamps(req.query);
    } catch (err) {
        logger.error("Could not get or validate timestamp. ", err);
        return res.status(400).send(err);
    }
    var intents = []
    try {
        intents = req.query.intents.split(',');
    } catch(err) {
        logger.error("Could not obtain intents from request query, Reason: ", err)
        return res.status(400).send("Did not find intents in request query");
    }
    var start_day_ts = moment.utc(time_obj.start_day).add(time_obj.utc_offset, 'minutes').toDate().getTime();
    var start_month_ts = moment.utc(time_obj.start_month).add(time_obj.utc_offset, 'minutes').toDate().getTime();
    var tz_offset_msecs = time_obj.utc_offset * 60 * 1000;
    var template_string = "SELECT count(distinct user_id) as count, " +
                          "DATE_TRUNC('period_to_replace', TIMESTAMP 'epoch' + ((created_at - " + tz_offset_msecs + ")/ 1000) * INTERVAL '1 second' ) AS created_interval " +
                          "FROM " + REDSHIFT_DB_TABLE + " " +
                          "WHERE bot_id = '" + req.params.bot_id + "' " +
                          "AND created_at BETWEEN start_timestamp_to_replace" + " AND " + time_obj.end_ts + " AND (";
    var intents_matcher_string = ""
    for (var i = 0; i < intents.length; ++i ) {
        intents_matcher_string += "intent_id = '" + intents[i] + "'";
        var suffix = (i == (intents.length - 1)) ? ") " : " OR ";
        intents_matcher_string += suffix;
    }
    template_string += intents_matcher_string;
    template_string += "GROUP BY created_interval ORDER BY created_interval;";
    var daily_query = template_string.replace('period_to_replace','day').replace('start_timestamp_to_replace', start_day_ts);
    var monthly_query = template_string.replace('period_to_replace','month').replace('start_timestamp_to_replace', start_month_ts);
    var total_query = "SELECT count(distinct user_id) as count FROM " + REDSHIFT_DB_TABLE + " " +
                       "WHERE bot_id = '" + req.params.bot_id + "' AND ( " +
                       intents_matcher_string +
                       " AND created_at BETWEEN " + time_obj.start_ts + " AND " + time_obj.end_ts;
    var extract_data_fn = function(data, period) {
        result = {}
        if (period == 'total') {
            return data.rows[0]['count']
        }
        moment_offset = {
            daily: 10,
            monthly: 7
        }[period]
        data.rows.forEach(function(row){
            key = moment(row['created_interval']).format().substr(0, moment_offset);
            result[key] = row['count'];
        });
        return result;
    }
    makeParallelQueriesForDifferentTimePeriods(
        {
            daily: {
                query_string: daily_query,
                extract_data_fn: extract_data_fn
            },
            monthly: {
                query_string: monthly_query,
                extract_data_fn: extract_data_fn
            },
            total: {
                query_string: total_query,
                extract_data_fn: extract_data_fn
            }
        },
        function(err, results) {
            if (err) {
                logger.error("Error during one of the queries. Reason: ", err)
                return res.status(500).send(err);
            } else {
                logger.info("Sending unique_users_for_intents info to caller")
                return res.status(200).send(results);
            }
        }
    )
}

// Function to make queries to obtain daily/monthly results in parallel
// param time_periods - info for each time period - contains daily, monthly
// param callback - callback fn.
function makeParallelQueriesForDifferentTimePeriods(time_periods, callback) {
    pg_pool.connect(function(err, client, client_done) {
        if (err) {
            logger.error("Could not connect to redshift. Reason: ", err);
            return res.status(500).send("Could not connecto to redshift: ", err);
        }
        async.parallel({
            daily_metrics: function(cb) {
                var daily = time_periods.daily;
                logger.info("Daily query string: ", daily.query_string);
                client.query(daily.query_string, function(err, data) {
                    if (err) {
                        logger.error("Could not get daily metrics. Reason: ", err);
                        return cb(err);
                    }
                    return cb(null, daily.extract_data_fn(data, 'daily'));
                })
            },
            monthly_metrics: function(cb) {
                var monthly = time_periods.monthly;
                logger.info("Monthly query string: ", monthly.query_string);
                client.query(monthly.query_string, function(err, data) {
                    if (err) {
                        logger.error("Could not get monthly metrics. Reason: ", err);
                        return cb(err);
                    }
                    return cb(null, monthly.extract_data_fn(data, 'monthly'));
                })
            },
            total_metrics: function(cb) {
                if (time_periods.total) {
                    var total = time_periods.total;
                    logger.info("Total query string: ", total.query_string);
                    client.query(total.query_string, function(err, data) {
                        if (err) {
                            logger.error("Could not get total metircs. Reason: ", err);
                            return cb(err)
                        }
                        return cb(null, total.extract_data_fn(data, 'total'));
                    })
                } else {
                    cb(null, null);
                }
            }
        }, function(err, results) {
            client_done(null); // Release the conection back to the pool.
            if (err) {
                return callback(err);
            } else {
                return callback(null, {
                    daily: results.daily_metrics,
                    monthly: results.monthly_metrics,
                    total: results.total_metrics
                });
            }
        })
    })
}