/* eslint-disable no-underscore-dangle */
var request     = require('request');
var logger      = require('../../common/utils/logger'); 
var querystring = require('querystring');
var async       = require('async');
var constants   = require('../../common/constants');
var BotConfigDB = require('../../common/models/bot_config');

const ES_ENDPOINT = "https://search-es-poc-hgc72se32bfckufzpm2omsj3ti.us-east-1.es.amazonaws.com/";
const NLP_WORD2VEC_ENDPOINT = "/word_vectors?message=";
const NLP_RERANK_ENDPOINT = "/rerank_kb"

const UPDATED_AT_MAPPING = {
    "type": "long",
}

const SORT_BY_DATE_QUERY = {"sort": [{ "updated_at": { "order": "desc", "unmapped_type": "date"}}]}

const KB_DOCS_MAPPING =
{
    "properties": {
        "description": {
            "type": "string",
            "fields": {
                "english": {
                    "type":     "string",
                    "analyzer": "english"
                }
            }
        },
        "image_url": {
            "type": "string",
            "fields": {
                "english": {
                    "type":     "string",
                    "analyzer": "english"
                }
            }
        },
        "title": {
            "type": "string",
            "fields": {
                "english": {
                    "type":     "string",
                    "analyzer": "english"
                }
            }
        },
        "title_vectors": {
            "type": "float",
            "index" : "no"
        },
        "url": {
            "type": "string",
            "fields": {
                "english": {
                    "type":     "string",
                    "analyzer": "english"
                }
            }
        },
        "updated_at": UPDATED_AT_MAPPING,
        "tags": {
            "type": "string",
            "fields": {
                "english": {
                    "type": "string",
                    "analyzer": "english"
                }
            }
        }
    }
}

const ENGLISH_ANALYZER_SETTINGS = {
    "analysis": {
        "analyzer": {
            "english": {
                "filter": [
                    "english_possessive_stemmer",
                    "lowercase",
                    "english_stop",
                    "english_keywords",
                    "english_stemmer"
                ],
                "tokenizer": "standard"
            }
        },
        "filter": {
            "english_keywords": {
                "keywords": [
                    ""
                ],
                "type": "keyword_marker"
            },
            "english_possessive_stemmer": {
                "language": "possessive_english",
                "type": "stemmer"
            },
            "english_stemmer": {
                "language": "english",
                "type": "stemmer"
            },
            "english_stop": {
                "stopwords": [
                    "i",
                    "me",
                    "my",
                    "myself",
                    "we",
                    "our",
                    "ours",
                    "ourselves",
                    "your",
                    "yours",
                    "yourself",
                    "yourselves",
                    "he",
                    "him",
                    "his",
                    "himself",
                    "she",
                    "her",
                    "hers",
                    "herself",
                    "it",
                    "its",
                    "itself",
                    "they",
                    "them",
                    "their",
                    "theirs",
                    "themselves",
                    "what",
                    "which",
                    "who",
                    "whom",
                    "how",
                    "this",
                    "that",
                    "these",
                    "those",
                    "am",
                    "is",
                    "are",
                    "was",
                    "were",
                    "be",
                    "been",
                    "being",
                    "have",
                    "has",
                    "had",
                    "having",
                    "do",
                    "does",
                    "did",
                    "doing",
                    "a",
                    "an",
                    "the",
                    "and",
                    "but",
                    "if",
                    "or",
                    "because",
                    "as",
                    "until",
                    "while",
                    "of",
                    "at",
                    "by",
                    "for",
                    "with",
                    "about",
                    "against",
                    "between",
                    "into",
                    "through",
                    "during",
                    "before",
                    "after",
                    "above",
                    "below",
                    "to",
                    "from",
                    "up",
                    "down",
                    "in",
                    "out",
                    "on",
                    "off",
                    "over",
                    "under",
                    "again",
                    "further",
                    "then",
                    "once",
                    "here",
                    "there",
                    "when",
                    "all",
                    "any",
                    "both",
                    "each",
                    "few",
                    "more",
                    "most",
                    "other",
                    "some",
                    "such",
                    "only",
                    "own",
                    "same",
                    "so",
                    "than",
                    "too",
                    "very",
                    "can",
                    "will",
                    "just",
                    "should",
                    "now"
                ],
                "type": "stop"
            }
        }
    }
}

var esClient = require('elasticsearch').Client({
  hosts: ES_ENDPOINT,
  connectionClass: require('http-aws-es'),
  amazonES: {
    region: 'us-east-1',
    accessKey: process.env.AWS_ACCESS_KEY_ID,
    secretKey: process.env.AWS_SECRET_ACCESS_KEY
  }
});


var ElasticSearchWrapper = module.exports;
const MAX_GET_RESULTS=1000;

// Get all docs for a bot
ElasticSearchWrapper.getAllDocsForBot = function (req, res) {
    esClient.search({
        index: req.bot._id,
        type: req.params.doc_type,
        body: SORT_BY_DATE_QUERY,
        size: MAX_GET_RESULTS,
    }, (err, documents) => {
        if (err) {
            return res.status(constants.RUNTIME_ERROR).send(`Could not get all docs from ES for
                              bot ${req.bot._id} of type ${req.params.doc_type}. err: ${err}`);
        }
        if (!documents.hits || !documents.hits.hits) {
            return res.status(constants.OK).send([]);
        }
        const retArray = documents.hits.hits.map(doc => Object.assign({ id: doc._id }, doc._source));

        return res.status(constants.OK).send(JSON.stringify(retArray));
    });
};

// Create index for a bot
function createIndex(bot_id, cb) {
    const url = ES_ENDPOINT + bot_id
    esClient.indices.create({
        index: bot_id,
        body: {
            settings: ENGLISH_ANALYZER_SETTINGS
        }
    }, function(err, response, status) {
        status_code = (status || constants.RUNTIME_ERROR)
        if (err || status_code != constants.OK) {
            logger.error("Could not create index for bot: ", bot_id,
                         "err = ", err, "status code = ", status_code, "response = ", response)
            return cb("Could not create index for bot")
        } else {
            logger.info("Created index for bot ", bot_id)
            return cb(null)
        }
    })
}

// Fetch title vectors by calling the corresponding algo endpoint
function fetchWordVectors(text, cb) {
    const url = process.env.NLP_SERVER + NLP_WORD2VEC_ENDPOINT + text
    logger.info(`Calling algo to get word vectors, URL = ${url}`)
    
    request(url, function (err, response, body) {
        if (err || ((response || {}).statusCode != constants.OK)) {
            logger.error("Could not get word vectors, err = ", err, "response = ", response)
            return cb(err)
        }
        
        var results_json = JSON.parse(body);
        if (results_json && results_json.word_vectors) {
            return cb(null, results_json.word_vectors)
        } else {
            return cb("ERROR: Empty word vectors", null)  
        }
    })
}

// Create an index and doc type for a bot if it does not already exist.
// See api/test/sample_mapping.json for sample mapping.
// If writing based a CSV file, it makes sense to generate the mapping dynamically
// based on the contents of the csv file.
// Note: context needs to have the following params defined
//       bot_id
//       document_type
ElasticSearchWrapper.checkAndCreateIndexAndDocTypeForBotInternal = function(context, next) {
    const bot_id = context.bot_id;
    const doc_type = context.doc_type;
    const mapping = context.mapping;
    if (!mapping.properties.updated_at) {
        mapping.properties.updated_at = UPDATED_AT_MAPPING;
    }
    async.waterfall([
        function(cb) {  // Check if index exists. If not create it.
            const url = ES_ENDPOINT + bot_id
            esClient.indices.exists({index: bot_id}, function(err, response, status) {
                status_code = (status || constants.RUNTIME_ERROR)
                if (err || status_code != constants.OK) {
                    logger.info(`Could not find index for bot ${bot_id}. Creating it..`)
                    createIndex(bot_id, function(err) {
                        if(err) {
                            const message = `Could not create index. Reason: ${err}`;
                            logger.error(message);
                            return cb({status: constants.RUNTIME_ERROR, message: message});
                        } else {
                            return cb(null);
                        }
                    })
                } else {
                    logger.info("Index for bot ", bot_id, "Already exists.")
                    return cb(null);
                }
            })
        },
        function(cb) {  // Check if doc_type with mapping exists. If not create it.
            esClient.indices.existsType({index: bot_id, type: doc_type }, function(err, response, status) {
                var status_code = (status || constants.RUNTIME_ERROR);
                if (err || status_code != constants.OK) { // index does not exist. Create one with the desired mapping.
                    logger.info(`Mapping does not exist for doc_type ${doc_type}. Creating it`);
                    logger.info(`Creating this mapping: ${JSON.stringify(mapping)}`);
                    esClient.indices.putMapping(
                        {
                            index: bot_id,
                            type: doc_type,
                            body: mapping
                        }, function(err, response, status) {
                        status_code = (status || constants.RUNTIME_ERROR)
                        if (status_code != constants.OK) {
                            logger.error("Could not create mapping for bot_id ", bot_id,
                                         "and document type ", doc_type, " error = ", err,
                                         " Status code = ", status_code, "Response body = ", response);
                            return next({status: status_code, message: response});
                        } else {
                            logger.info(`Mapping created for document_type: ${doc_type} ` +
                                        `for bot_id: ${bot_id}`);
                            return next(null, `Mapping created successfully for doc_type: ${doc_type}` +
                                              `for bot_id: ${bot_id}`);
                        }
                    })
                } else {
                    logger.info("Mapping already exists.")
                    return next(null, "Mapping already exists.");
                }
            })
        }
    ], function(err, result) {
        if (err) {
            logger.error("Error when creating index/doc_type mapping for bot ", bot_id,
                         " and doc_type ", doc_type, ". Error = ", err)
            next(err);
        } else {
            next(null);
        }
    })
}

ElasticSearchWrapper.checkAndCreateIndexAndDocTypeForBot = function(req, res, next) {
    const doc_type = req.params.doc_type;
    const mapping = doc_type.startsWith('kb_docs') ? KB_DOCS_MAPPING : req.body;

    ElasticSearchWrapper.checkAndCreateIndexAndDocTypeForBotInternal(
        {
            bot_id: req.bot._id,
            doc_type: doc_type,
            mapping: mapping
        },
        function(err, response_string) {
            if (err) {
                res.status(err.status).send(err.message);
            } else {
                res.status(constants.CREATED).send(response_string);
            }
            next();
        }
    )
}

// Handle kb_docs creation slightly differently.
// In particular.
// 1. Validate that there is title in the body
// 2. If the index or mapping does not exist, then create it
//    based on the hard coded kb docs mapping.
// context should contain
//   bot_id
//   doc_type
//   document_data
function handle_kb_docs_mapping(context, next) {
    // Validate that the req must have at least a title
    if (!context.document_data.title) {
       return next({status: constants.BAD_REQUEST, message: "Request should have a 'title' field for kb_docs"});
    }
    ElasticSearchWrapper.checkAndCreateIndexAndDocTypeForBotInternal(
        {
            bot_id: context.bot_id,
            doc_type: context.doc_type,
            mapping: KB_DOCS_MAPPING
        },
        function(err, response_string) {
            return next(err);  // response_string will not be consumed
        }
    )
}

// Given a bot id, fetch its config and settings from Mongo DB
function fetchBotConfig(bot_id, next) {
    logger.info('Fetching bot config from Mongo DB');
    BotConfigDB.findOne({_id: bot_id}, {}, function(err, bot_config) {
        if (err || !bot_config) {
            return next(`Did not find bot config for ${bot_id}, error = ${err}`);
        } else {
            return next(null, bot_config);
        }
    });
}

// Given a ES ranking, only keep the top-k hits and discard the rest
function getTopK(ranking, topK, next) {
    if (topK < 1) {
        logger.error('topK cannot be less than 1, returning the original object');
    } else {
        if (ranking.hits && ranking.hits.hits && topK < ranking.hits.hits.length) {
            logger.info('slicing top-K results from original ranking');
            ranking.hits.hits = ranking.hits.hits.slice(0, topK); // keep only top K hits
            ranking.hits.total = topK;  // update the total field as well
        }
    }
    return next(null);  // no need to return anything because the array replacement has already been done
}

// Re-rank KB docs with word similarties, by calling algo endpoint 
function handleKBDocsReRanking(bot_id, search_body, orig_ranking, next) {
    const topK = 10;
    const query_msg = search_body.query.multi_match.query;
    const rerank_url = process.env.NLP_SERVER + NLP_RERANK_ENDPOINT;
    logger.info(`KB results re-ranking with query message: ${query_msg}`);
    logger.info(`Calling algo endpoint: ${rerank_url}`);

    async.waterfall([
        function(cb) {
            // two tasks: slice topK of original ranking && fetch bot config from mongo
            async.parallel({
                getTopK: function(callback) {
                    return getTopK(orig_ranking, topK, callback);
                },
                botConfig: function(callback) {
                    return fetchBotConfig(bot_id, callback);
                }
            }, cb);
        },
        function(results, cb) {
            // if the bot has environment variable, conduct the re-ranking
            const botEnv = BotConfigDB.getEnvVariables(results.botConfig);

            if (botEnv.kb_word2vec_ranking && botEnv.kb_word2vec_ranking == 1) {
                request.post({
                    url: rerank_url,
                    headers: {"Content-Type": "application/json"},
                    body: {
                        bot_id,
                        query: query_msg,
                        results: orig_ranking
                    },
                    json: true
                }, function (err, response, body) {
                    if (err || (response || {}).statusCode != constants.OK) {
                        const err_msg = err ? err : `Response not OK: ${response.statusCode}`;
                        return cb(err_msg);
                    } else {
                        logger.info(`Re-ranking succeeded`);
                        return cb(null, body); 
                    }
                })
            } else {
                logger.warn("bot not configured to do re-ranking, returning original ranking");
                return cb(null, orig_ranking); 
            }   
        }
    ], function(err, result) {
        if (err) {
            logger.warn(`Re-ranking failed, returning original ranking, error message = ${err}`);
            result = orig_ranking;
        }
        try {
            const kb_hits = result.hits.hits;
            for (var i in kb_hits) {
                delete kb_hits[i]._source.title_vectors;
            }
        } catch(e) {
            logger.error(`Error trying to delete the title vectors, reason = ${e}`);
        }
        return next(null, result);
    });
}

// Create or update a document based on id
// context must contain
//    bot_id
//    doc_type
//    document_data
ElasticSearchWrapper.createDocForBotInt = function(context, next) {
    // if id is not present then we create a document without an ID and let ES
    // create the id.
    // If it is, then we use the id to create the document
    // Always have an updated_at field for all documents
    if (!context.document_data.updated_at) {
        context.document_data.updated_at = Date.now();
    }
    const params = {
        index: context.bot_id,
        type: context.doc_type,
        body: context.document_data
    };
    
    async.waterfall([
        function(cb) {
            if (params.type.startsWith('kb_docs')) {
            // We do additional validations for kb_docs
                return handle_kb_docs_mapping(context, cb);
            } else {
                return cb(null);
            }
        },
        function(cb) {
            // If we don't have botConfig fetch it.
            if (!context.bot_config) {
                return fetchBotConfig(context.bot_id, cb);
            }
            return cb(null, context.bot_config);
        },
        function(bot_config, cb) {
            const botEnv = BotConfigDB.getEnvVariables(bot_config);
            if (params.type.startsWith('kb_docs') && botEnv && botEnv.kb_word2vec_ranking == 1 
                && params.body && params.body.title) {
                // if it is KB doc with flag on, we take the title and call algo to fetch the word vectors
                return fetchWordVectors(params.body.title, function(err, results) {
                    if(err) {
                        const message = `Error in fetching title vectors. Reason: ${err}`;
                        logger.error(message);
                        return cb(message);
                    } else {
                        params.body.title_vectors = results;
                        return cb(null);
                    }    
                })
            } else if (params.type.startsWith('kb_docs') && (botEnv || {}).kb_word2vec_ranking != 1) {
                logger.warn(`Word vector endpoint was not called on kb_doc`);
                return cb(null);
            } else {
                return cb(null);
            }
        },
        function(cb) {
            // if body does not contain id, the create the document.
            // if it does contain id, then update the document.
            if (params.body.id && !/^\s*$/.test(params.body.id)) {
                params.id = params.body.id;
                delete params.body.id;
            }
            esClient.index(params, function(err, results, status) {
                if (err || !results || !results._id) {
                  status_code = (status || constants.RUNTIME_ERROR)
                  logger.error("Could not create/update the  document in elastic search: Error: ", err,
                               "Results: ", JSON.stringify(results, null, 4), " Status code = ", status_code);
                  return cb({status: status_code, message: err});
                } else {
                  logger.info(`Created document of type ${params.type} for bot  ${params.index}  with id ${results._id}`);
                  return next(null, {id: results._id});
                }
            })
        }], function(err) {
        return next(err);
    })
}

ElasticSearchWrapper.createDocForBot = function(req, res, next) {
    ElasticSearchWrapper.createDocForBotInt(
        {
            bot_id: req.bot._id,
            doc_type: req.params.doc_type,
            bot_config: req.bot,
            document_data: req.body
        },
        function(err, response_message) {
            if (err) {
                res.status(err.status).send(err.message);
                next(err)
            } else{
                 res.status(constants.CREATED).send(response_message)
                 next(null)
            }
        }
    )
}
// Delete a doc for a bot
ElasticSearchWrapper.deleteDocForBot = function(req, res) {
    const url = ES_ENDPOINT + req.bot._id + "/" + req.params.doc_type + "/" + req.params.doc_id
    params = {index: req.bot._id, type: req.params.doc_type, id: req.params.doc_id}
    esClient.delete(params, function(err, results, status) {
        var status_code = status || constants.RUNTIME_ERROR;
        if (err) {
            logger.warn("Coud not delete the document in elastic search for bot: ", req.bot._id, " Reason: ", err);
            return res.status(status_code).send(err);
        } else {
            logger.info("Deleted document of type ", req.params.doc_type, " with id ", req.params.doc_id);
            return res.status(status_code).send(results);
        }
    })
}

// Get a document for a bot
ElasticSearchWrapper.getDocForBot = function (req, res) {
    esClient.get({
        index: req.bot._id,
        type: req.params.doc_type,
        id: req.params.doc_id,
    }, (err, document) => {
        if (err) {
            logger.error(`Could not get doc ${req.params.doc_id} of type ${req.params.doc_type} of bot
                          ${req.bot._id}. err: ${err}`);
            return res.status(constants.RUNTIME_ERROR).send(err);
        }
        if (!document.found) {
            return res.status(constants.NOT_FOUND).send(`Did not find document ${req.params.doc_id}`);
        }
        const retObj = {
            id: document._id,
        };
        Object.assign(retObj, document._source);
        return res.status(constants.OK).send(JSON.stringify(retObj, null, 4));
    });
};

// Internal method to edit a document.
ElasticSearchWrapper.editDocForBotInt = function(bot_id, doc_type, doc_id, updated_content, next) {
    logger.info(`Editing doc ${doc_id} of type ${doc_type} of bot ${bot_id}`);
    updated_content.updated_at = Date.now();
    esClient.update({
        index: bot_id,
        type: doc_type,
        id: doc_id,
        body: {doc: updated_content}
    }, function(err, response) {
        if (err) {
            logger.error(`Error when updating doc ${doc_id} of type ${doc_type} of bot ${bot_id}. Reason :${err}`);
            next(null, response);
        } else {
            logger.info(`Successfully updated doc ${doc_id} of type ${doc_type} of bot ${bot_id}.`);
            next(err);
        }
    });
}

// Edit doc for bot.
ElasticSearchWrapper.editDocForBot = function(req, res) {
    if (req.params.doc_type.startsWith('kb_docs') && !req.body.title) {
        res.status(constants.BAD_REQUEST).send('Document should have a title field for kb_docs');
    }
    ElasticSearchWrapper.editDocForBotInt(req.bot._id, req.params.doc_type, req.params.doc_id, req.body, function(err, response) {

        if (err) {
            res.status(constants.RUNTIME_ERROR).send(err);
        } else {
            res.status(constants.OK).send(response);
        }
    })
}

// Internal method to search elastic search based on search_body
ElasticSearchWrapper.searchDocInt = function(bot_id, doc_type, search_body, next) {
    async.waterfall([
        function(cb) {
            esClient.search({
                index: bot_id,
                type: doc_type,
                body: search_body,
                searchType: 'dfs_query_then_fetch',
                size: 100
            }, function (err, response) {
                if (err) {
                    logger.error(`Error in calling elastic search API, reason = ${err}`);
                    return cb(err, null);
                } else {
                    return cb(null, response);
                }
            });
        },
        function(ranking, cb) {
            if (doc_type.startsWith('kb_docs')) {
                return handleKBDocsReRanking(bot_id, search_body, ranking, cb);
            } else {
                return cb(null, ranking);
            }
        }
    ], function (err, response) {
        return next(err, response);
    })
}

// Search elastic search based on the body.
// TODO: remove this method from c3po once it is moved to Yaaris
ElasticSearchWrapper.searchDoc = function(req, res) {
    ElasticSearchWrapper.searchDocInt(
        req.bot._id,
        req.params.doc_type,
        req.body,
        function(err, response) {
            if (err) {
                return res.status(constants.RUNTIME_ERROR).send(err);
            } else {
                return res.status(constants.OK).send(response);
            }
        }
    );
}

// Delete all documents in a particular doc_type in a bot internal method
ElasticSearchWrapper.deleteDocTypeForBotInt = function(bot_id, doc_type, next) {
    logger.info(`Deleting doc type ${doc_type} of bot ${bot_id} from elastic search.`)
    esClient.deleteByQuery(
        {
            index: bot_id,
            type: doc_type,
            body: {
                query: {match_all: {}}
            }
        }, function(err, response) {
            if (err) {
                logger.error(`Could not delete doc type ${doc_type} of bot ${bot_id} from elastic search. Reason: ${err}`);
                next(err);
            } else {
                logger.info(`Successfully deleted doc type ${doc_type} of bot ${bot_id} from elastic search.`)
                next(null, response);
            }
        }
    );
}

// Delete all documents in a doc_type external method.
ElasticSearchWrapper.deleteDocTypeForBot = function(req, res, next) {
    ElasticSearchWrapper.deleteDocTypeForBotInt(req.bot._id, req.params.doc_type, function(err, response) {
        if(err) {
            res.status(constants.RUNTIME_ERROR).send(err);
        } else {
            return res.status(constants.OK).send(response);
        }
    })
}
