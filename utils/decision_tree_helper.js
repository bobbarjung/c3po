/* eslint-disable no-underscore-dangle */
/* eslint-disable no-plusplus */
const constants = require('../../common/constants');
const async = require('async');
const csv = require('csv-parser');
const fs = require('fs');
const DTree = require('../../common/models/decision_tree');
const logger = require('../../common/utils/logger');
const Intent = require('../../common/models/intent');
const aws = require('aws-sdk');
const uuidV1 = require('uuid/v1');
const EsWrapper = require('./es_wrapper');
const path = require('path');

const DTreeStates = {
    processing: 'processing',
    active: 'active',
    error: 'error',
};

// Dont store more than 20 values for a key in mongo.
// If an attribute has more than 20 values, it is probbaly not something that will be
// used for decisions.
const MAX_DTREE_VALUES_LEN = 20;


const DecisionTreeHelper = module.exports;

// Return the Decision tree structure.
DecisionTreeHelper.getDecisionTree = function (req, res) {
    const getTreeFromId = function (treeId) {
        DTree.findOne({ _id: treeId }, (err, treeDoc) => {
            if (err) {
                const msg = `Could not retrieve tree ${treeId} from mongo. reason: ${err}`;
                logger.error(msg);
                return res.status(constants.RUNTIME_ERROR).send(msg);
            }
            if (!treeDoc) {
                // Did not find tree_doc
                if (req.params.treeId) {
                    // user specified a tree_id, so return NOT_FOUND
                    return res.status(constants.NOT_FOUND).send('Did not find dtree');
                }
                return res.status(constants.OK).send([]);
            }
            return res.status(constants.OK).send(JSON.stringify(treeDoc, null, 4));
        });
    };
    if (req.params.treeId) {
        // If params has tree_id, then just read the document from mongo and we
        // are done.
        return getTreeFromId(req.params.treeId);
    }
    // If not, then we revert to the first element in the decision_trees array
    // of the intent.
    return async.waterfall([
        function (cb) {
            if (!req.intent) {
                return Intent.findOne({ _id: req.intent_id }, (err, intentDoc) => cb(null, intentDoc));
            }
            return cb(null, req.intent);
        },
        function (intentDoc) {
            const treeId = (intentDoc.decision_trees || [])[0];
            if (!treeId) {
                logger.warn('Intent ', req.intent_id, 'Has no decision trees');
                return res.status(constants.OK).send([]);
            }
            return getTreeFromId(treeId);
        },
    ]);
};

// Performs validations on a new Decision tree.
// intent_id is in req.intend_id
function validateNewDecisionTree(req, res, context, next) {
    const body = req.body;
    // Name is present
    if (!body.name) {
        return res.status(constants.BAD_REQUEST).send('name should be specified');
    }

    // Datafile is present
    if (!body.data_file_path) {
        return res.status(constants.BAD_REQUEST).send('data file path should be specified');
    }

    // Intent does not alredy have a decision tree.
    return Intent.findOne({ _id: req.intent_id }, (err, intentDoc) => {
        if ((intentDoc.decision_trees || [])[0]) {
            // return res.status(constants.CONFLICT).send('Intent already has a decision tree.');
            context.intent = intentDoc;
            return next(null, context);
        }
        context.intent = intentDoc;
        return next(null, context);
    });
}

// validate the attributes specified as part of an update to the dtree.
DecisionTreeHelper.validateAttributes = function (attributes) {
    // Check if attributes is provided, then any image_attribute specified is also an attribute
    const attributeNames = attributes.map(attribute => attribute.attribute);

    attributes.forEach((attribute) => {
        if (!attribute.image_attribute) {
            return;
        }
        if (attributeNames.indexOf(attribute.image_attribute) === -1) {
            throw new TypeError(`image attribute ${attribute.image_attribute} is not one of the attributes`);
        }
    });
};

// Create the Dtree object in mongo.
// context.intent has to be defined for this method.
function createDTreeObject(req, res, context, next) {
    const dtree = new DTree();
    const intent = context.intent;
    dtree.name = req.body.name;
    dtree.description = req.body.description || req.body.name;
    dtree.status = DTreeStates.processing;
    dtree.data_file = {
        name: req.body.data_file_path,
        last_uploaded: new Date(),
    };
    dtree.save((err, dtreeDoc) => {
        if (err) {
            logger.error('Could not write Dtree ', req.body.name, ' to mongodb. Reason: ', err);
            return res.status(constants.RUNTIME_ERROR).send(err);
        }
        logger.info('new decision tree with id ', dtreeDoc._id, ' was created. ');
        intent.decision_trees = [dtreeDoc._id];
        return Intent.update({ _id: intent.id }, { decision_trees: [dtreeDoc._id] }, (err1) => {
            if (err1) {
                logger.error('Could not update intent with decision tree ', dtreeDoc._id, ' Reason: ', err1);
                return res.status(constants.RUNTIME_ERROR).send(err1);
            }
            logger.info('Intent ', intent._id, ' updated with dtree info.');
            // At this point we respond back the API caller, and do rest of the processing
            // asynchronously.
            res.status(constants.OK).send(JSON.stringify(dtreeDoc));
            context.dtree = dtreeDoc;
            return next(null, context);
        });
    });
}
// Update the dtree object.
function updateDTreeObject(req, res, context, next) {
    const treeId = req.params.tree_id;
    const updatedObj = {};

    if (req.body.name) {
        logger.info(`Updating name of dtree ${treeId} object to ${req.body.name}`);
        updatedObj.name = req.body.name;
    }
    if (req.body.description) {
        logger.info(`Updating description of dtree ${treeId} object to ${req.body.description}`);
        updatedObj.description = req.body.description;
    }
    if (req.body.attributes && Object.keys(req.body.attributes).length !== 0) {
        logger.info(`Updating attributes of dtree ${treeId} object to ${JSON.stringify(req.body.attributes, null, 4)}`);
        try {
            DecisionTreeHelper.validateAttributes(req.body.attributes);
        } catch (ex) {
            logger.error(`Could not validate attributes for dtree ${treeId}. ${ex}`);
            res.status(constants.BAD_REQUEST).send(ex);
            next(ex);
        }
        updatedObj.attributes = req.body.attributes;
    }
    if (Object.keys(updatedObj).length === 0) {
        logger.info(`Nothing changed in dtree ${treeId} object. Nothing to update.`);
    }

    DTree.update({ _id: treeId }, updatedObj, (err) => {
        if (err) {
            const message = `Could not update the decision tree ${treeId} in mongodb. Reason: ${err}`;
            logger.error(message);
            return res.status(constants.RUNTIME_ERROR).send(message);
        }
        const message = `Successfully updated the decision tree ${treeId} in mongodb`;
        logger.info(message);
        res.status(constants.OK).send(message);
        return next(null, context);
    });
}

// @param - attributes - An object that contains information about attributes
// @param - dtree - The dtree
// @param - next - The callback
function saveDtreeAttributes(attributes, dtree, next) {
    // Update mongo
    // flatten attributes dictionary into an array
    let attributesArray = [];
    if (!Array.isArray(attributes)) {
        Object.keys(attributes).forEach((key) => {
            attributesArray.push(attributes[key]);
        });
    } else {
        attributesArray = attributes;
    }
    // Update mongo.
    DTree.update({ _id: dtree._id }, { attributes: attributesArray }, (err) => {
        if (err) {
            logger.error(`Could not update dtree ${dtree._id}  in mongo. Reason: ${err}`);
            return DTree.update({ _id: dtree._id }, { status: DTreeStates.error }, (err1) => {
                logger.info(`Set the status for dtree ${dtree._id}  in mongo to error`);
                return next(err1);
            });
        }
        logger.info('Updated the Dtree object with attributes array ');
        return next(null);
    });
}

function extractDecisionAttributesFromJson(req, res, context, next) {
    const attributes = {};
    const dtree = context.dtree;
    if (!dtree) {
        return next(`Context does not contain dtree for dtree ${req.params.tree_id}`);
    }
    logger.info(JSON.stringify(dtree, null, 4));
    (dtree.attributes || []).forEach((attr) => {
        attributes[attr.attribute] = attr;
    });
    const jsonArray = context.json_data;
    if (!jsonArray) {
        return next(`Could not find json data for dtree ${dtree._id}`);
    }

    return async.waterfall([
        function (cb) {
            // associate an order count with each entry.
            // populate the attribute values
            jsonArray.forEach((data) => {
                Object.keys(data).forEach((key) => {
                    const value = data[key];
                    // When storing attributes in mongo, only store attributes whose values
                    // are primitive types - i.e skip array and objects
                    // However we will store the entire document as is when storing in ES.
                    if (!!value && (value.constructor === Object || value.constructor === Array)) {
                        return;
                    }
                    if (!value) {
                        return;
                    }
                    if (!(key in attributes)) {
                        logger.info(`Added attribute ${key}`);
                        attributes[key] = {
                            attribute: key,
                            values: [],
                            enabled: false,
                            question: undefined,
                        };
                    }
                    if (attributes[key].values.length > MAX_DTREE_VALUES_LEN) {
                        return;
                    }
                    if (!attributes[key].values.includes(value)) {
                        attributes[key].values.push(value);
                    }
                });
            });
            // Remove attributes that have no values
            Object.keys(attributes).forEach((attribute) => {
                if (attributes[attribute].values.length === 0) {
                    delete (attributes[attribute]);
                }
            });
            return cb(null);
        },
        function (cb) {
            // Create documents in Elastic Search
            let dtreeOrder = 0;
            async.each(jsonArray, (data, eachCb) => {
                EsWrapper.createDocForBotInt(
                    {
                        bot_id: req.bot._id,
                        doc_type: `dtree-${dtree._id}`,
                        bot_config: req.bot,
                        document_data: Object.assign(data, { dtree_order: dtreeOrder++ }),
                    },
                    (err) => {
                        if (err) {
                            logger.error(`Could not write document ${data} to ES. Reason: ${err}`);
                            return eachCb(err);
                        }
                        return eachCb(null);
                    }
                );
            }, (err) => {
                if (err) {
                    logger.error(`Could not write one or more dtree entries to ES. ${err}`);
                    return cb(err);
                }
                logger.info(`Finished writing all documents to ES for dtree ${dtree._id}`);
                return cb(null);
            });
        },
        function (cb) {
            saveDtreeAttributes(attributes, dtree, cb);
        },
    ],
    (err) => {
        if (err) {
            logger.error(`Could not extract dtree attributes from json file for dtree ${dtree._id}. ${err}`);
            return next(err);
        }
        logger.info(`Successfully extracted attributes from JSON for dtree ${dtree._id}`);
        return next(null, context);
    });
}

function extractDecisionAttributesFromJsonFile(req, res, context, next) {
    // read the jsonArray from file
    const filePath = context.file_path;
    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            logger.error(`Could not read contents of dtree json file. ${err}`);
            return next(err);
        }
        const jsonData = JSON.parse(data);
        if (!Array.isArray(jsonData)) {
            logger.error('Input dtree json file is not an array');
            return next('Input dtree json file is not an array');
        }
        context.json_data = jsonData;
        return extractDecisionAttributesFromJson(req, res, context, next);
    });
}

// Process a CSV file.
function extractDecisionAttributesFromCsvFile(req, res, context, next) {
    const csvFilePath = context.file_path;

    // Convert CSV to JSON.
    const jsonData = [];
    fs.createReadStream(csvFilePath)
        .pipe(csv())
        .on('data', (data) => {
            jsonData.push(data);
        })
        .on('end', () => {
            // Write to a json file.
            context.json_data = jsonData;
            extractDecisionAttributesFromJson(req, res, context, next);
        });
}

// Extract attributes and its values and store them in mongo.
// For now this only works on CSV
// TODO: also support xls.
// Note: context.dtree and context.file_path must be defined for this method.
function extractDecisionAttributes(req, res, context, next) {
    if (path.extname(context.file_path) === '.csv') {
        logger.info('Extracting decision tree from csv file');
        extractDecisionAttributesFromCsvFile(req, res, context, next);
    } else if (path.extname(context.file_path) === '.json') {
        logger.info('Extracting decsion tree from json file');
        extractDecisionAttributesFromJsonFile(req, res, context, next);
    } else {
        next('Only csv and json formats are currently supported for decision trees');
    }
}

// Read decision tree from s3.
// context.dtree has to be defined for this method.
function readDecisionTreeFromS3(req, res, context, next) {
    const dataFilePath = req.body.data_file_path;
    const dataFileExtension = path.extname(dataFilePath);
    const s3 = new aws.S3({ signatureVersion: 'v4' });
    logger.info('reading decision tree file ', req.body.data_file_path, ' from s3');
    const filePath = `./uploads/${uuidV1()}${dataFileExtension}`;
    const file = fs.createWriteStream(filePath);
    logger.info('Creating file ', filePath);
    file.on('close', () => {
        logger.info(`Finished reading s3 file and writing it to temp path ${filePath}`);
        context.file_path = filePath;
        return next(null, context);
    });
    file.on('error', (err) => {
        logger.error(`Could not read s3 file ${err}`);
        return next('Could not read s3 file: ', err);
    });
    const params = { Bucket: process.env.S3_PRIVATE_FILES_BUCKET_ID, Key: dataFilePath };
    s3.getObject(params).createReadStream().pipe(file);
}

// Update the dtree data file
function updateDTreeDataFile(req, res, context, next) {
    const treeId = req.params.tree_id;
    if (!req.body.data_file_path) {
        return next(null, context);
    }

    return async.waterfall([
        function (cb) {
            // Delete current entries in doc_type
            // TODO: replace this with atomic alias switch.
            EsWrapper.deleteDocTypeForBotInt(req.bot._id, `dtree-${treeId}`, cb);
        }, function (response, cb) {
            // read data file from s3.
            readDecisionTreeFromS3(req, res, context, cb);
        },
        function (ctx, cb) {
            // Extract Decision Attributes
            extractDecisionAttributes(req, res, ctx, cb);
        },
    ], (err, ctx) => {
        if (err) {
            logger.error(`Could not update data file for dtree ${treeId}. Reason: ${err}`);
            return next(err);
        }
        return next(null, ctx);
    });
}

// context.dtree must be defined
function setDTreeStatus(req, res, status, context, next) {
    const dtree = context.dtree;
    logger.info(`Setting dtree ${dtree._id} status to ${status}`);
    DTree.update({ _id: dtree._id }, { status }, (err) => {
        if (err) {
            logger.error('Could not set status of dtree ', dtree._id, ' to ', status, '. Reason: ', err);
        } else {
            logger.info('Set status of dtree ', dtree._id, ' to ', status, '.');
        }
        return next(null, context);
    });
}

// context.file_path must be defined.
function deleteTempDataFile(req, res, context, next) {
    const filePath = context.file_path;
    if (!filePath) {
        return next(null, context);
    }
    return fs.unlink(filePath, (err) => {
        if (err) {
            logger.warn('Could not delete temporary data file ', filePath, ' Reason: ', err);
        } else {
            logger.info('Deleted temporary data file ', filePath);
            next(null, context);
        }
    });
}

// Update the decision tree
DecisionTreeHelper.updateDecisionTree = function (req, res) {
    const context = {};
    const treeId = req.params.tree_id;
    logger.info(`Updating dtree ${treeId}`);
    async.waterfall([
        function (cb) {
            // load current attributes into mongo.
            DTree.findOne({ _id: treeId }, cb);
        }, function (treeDoc, cb) {
            context.dtree = treeDoc;
            setDTreeStatus(req, res, DTreeStates.processing, context, cb);
        },
        function (ctx, cb) {
            // Update the data file
            updateDTreeDataFile(req, res, ctx, cb);
        },
        function (ctx, cb) {
            // Update the Dtree object
            updateDTreeObject(req, res, ctx, cb);
        },
        function (ctx, cb) {
            // set Dtree Status to Active
            setDTreeStatus(req, res, DTreeStates.active, ctx, cb);
        },
        function (ctx, cb) {
            // delete TempData file
            deleteTempDataFile(req, res, ctx, cb);
        },
    ], (err) => {
        if (err) {
            logger.error(`Could not update decision tree. Reason ${err}`);
            return setDTreeStatus(req, res, DTreeStates.error, context, () => {});
        }
        return logger.info('Successfully updated the decision tree');
    });
};

// Create a new decision tree from an uploaded csv or xls file.
DecisionTreeHelper.createDecisionTree = function (req, res) {
    const context = {};
    async.waterfall([
        function (cb) { validateNewDecisionTree(req, res, context, cb); },
        function (ctx, cb) { createDTreeObject(req, res, ctx, cb); },
        function (ctx, cb) { readDecisionTreeFromS3(req, res, ctx, cb); },
        function (ctx, cb) { extractDecisionAttributes(req, res, ctx, cb); },
        function (ctx, cb) { setDTreeStatus(req, res, DTreeStates.active, ctx, cb); },
        function (ctx, cb) { deleteTempDataFile(req, res, ctx, cb); },
    ], (err) => {
        if (err) {
            logger.error('Could not create decision tree. Reason: ', err);
        } else {
            logger.info('Performed all the steps of creating a decision tree');
        }
    });
};

// Deletes a Dtree object from mongo
// 1. Deletes the tree document.
// 2. Deletes the tree_id pointer in the intent document
// The following need to be set
//    - context.intent
//    - context.tree_id
// Also tree_id should be in context.intent.decision_trees
function deleteDtreeObject(context, next) {
    async.waterfall([
        function (cb) {
            DTree.remove({ _id: context.tree_id }, (err) => {
                if (err) {
                    logger.error(`Could not delete the dtree document with id ${context.tree_id}. Reason: ${err}`);
                    cb(err);
                } else {
                    cb(null);
                }
            });
        },
        function (cb) {
            const intentTrees = context.intent.decision_trees;
            intentTrees.splice(intentTrees.indexOf(context.tree_id), 1);
            Intent.update({ _id: context.intent._id }, { decision_trees: intentTrees }, (err) => {
                if (err) {
                    logger.error(`Could not update the intent document. Reason: ${err}`);
                    cb(err);
                } else {
                    cb(null);
                }
            });
        },
    ], (err) => {
        if (err) {
            return next(err);
        }
        logger.info(`Deleted all mongo docs for dtree ${context.tree_id}`);
        return next(null);
    });
}

// The following needs to be set
//    req.intent - This is set by the validateTreeIdInIntent method in routes/bots.js
DecisionTreeHelper.deleteDecisionTree = function (req, res) {
    const context = {
        tree_id: req.params.tree_id,
        intent: req.intent,
        doc_type: `dtree-${req.params.tree_id}`,
    };
    logger.info(`Deleting dtree ${context.tree_id} of intent ${context.intent._id} of bot ${req.bot._id}`);
    async.waterfall([
        function (cb) {
            deleteDtreeObject(context, cb);
        },
        function (cb) {
            // Respond to the API after mongodb objects have been deleted.
            // ES objects can be deleted offline.
            res.status(constants.OK).send(`Dtree ${context.tree_id} was successfully deleted from mongo DB`);
            return cb(null);
        },
        function (cb) {
            EsWrapper.deleteDocTypeForBotInt(req.bot._id, context.doc_type, cb);
        },
    ], (err) => {
        if (err) {
            logger.error(`Some step in dtree ${req.params.tree_id} deletion failed. ${JSON.stringify(err, null, 4)}`);
        } else {
            logger.info(`All steps in delete of dtree ${req.params.tree_id} succeeded`);
        }
    });
};
