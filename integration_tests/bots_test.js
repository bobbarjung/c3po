/* eslint-disable no-underscore-dangle */
const async = require('async');
const logger = require('../../common/utils/logger');
const request = require('request');
const constants = require('../../common/constants');

const C3PO_SERVICE_URL = process.env.C3PO_SERVICE_URL;
const TEST_BOT_ID = '3070164780';
const headers = { 'X-Super-Tenant-Api-Key': process.env.SUPER_TENANT_API_KEY };

function getAllBots(next) {
    const botsUrl = `${C3PO_SERVICE_URL}/bots`;
    logger.info(`botsUrl = ${botsUrl}`);
    request({
        method: 'GET',
        uri: botsUrl,
        headers,
        json: true,
        accept: true,
    }, (err, code, botsResult) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Could not get all bots. err: ${err}.
                        code: ${code.statusCode}. botsResult: ${botsResult}`);
        }
        if (botsResult.length === 0) {
            return next('Retrieved 0 bots from API. Something is wrong');
        }
        logger.info(`Retrieved ${botsResult.length} active bots`);
        return next(null);
    });
}

function getBot(botId, next) {
    const botsUrl = `${C3PO_SERVICE_URL}/bots/${TEST_BOT_ID}`;
    request({
        method: 'GET',
        uri: botsUrl,
        headers,
        json: true,
        accept: true,
    }, (err, code, botsResult) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Could not get bot ${botId}. err: ${err}.
                        code: ${code.statusCode}. botsResult: ${botsResult}`);
        }
        if (botsResult._id !== botId) {
            return next('Did not retrieve the correct bot. Something is wrong');
        }
        logger.info(`Correctly retrieved the bot ${botId}`);
        return next(null);
    });
}

function runTest() {
    async.waterfall([
        function (cb) { getAllBots(cb); },
        function (cb) { getBot(TEST_BOT_ID, cb); },
    ], (err) => {
        if (err) {
            logger.error(`Hit an error during bots test. ${err}`);
            process.exit(1);
        }
        logger.info('Successfully passed bots test.');
        process.exit(0);
    });
}

runTest();
