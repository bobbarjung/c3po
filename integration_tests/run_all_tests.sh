set -e
node integration_tests/decision_trees_test.js
node integration_tests/es_docs_test.js
node integration_tests/bots_test.js
node integration_tests/intents_test.js
node integration_tests/knowledge_bases_test.js
