/* eslint-disable no-underscore-dangle */
const async = require('async');
const logger = require('../../common/utils/logger');
const request = require('request');
const constants = require('../../common/constants');
const fs = require('fs');
const sleep = require('sleep');

const C3PO_SERVICE_URL = process.env.C3PO_SERVICE_URL;
const API_SERVICE_URL = process.env.API_SERVICE_URL;
const TEST_BOT_ID = '3070164780';
const ES_DOCS_URL = `${C3PO_SERVICE_URL}/bots/${TEST_BOT_ID}/es_docs`;
const headers = { 'X-Super-Tenant-Api-Key': process.env.SUPER_TENANT_API_KEY };
const FAQ_DOCS_FILE_PATH = './integration_tests/jsons/es_test.json';

function readFaqDocsFile(context, next) {
    fs.readFile(context.faq_docs_file_path, 'utf8', (err, data) => {
        if (err) {
            return next(`Could not read json file ${context.faq_docs_file_path}`);
        }
        const jsonData = JSON.parse(data);
        context.faq_docs_mapping = jsonData.mapping;
        context.faq_docs = jsonData.docs;
        context.query = jsonData.query;
        logger.info(`mapping = ${JSON.stringify(context.faq_docs_mapping)}`);
        return next(null, context);
    });
}

function createMapping(context, next) {
    const esUrl = `${ES_DOCS_URL}/${context.doc_type}/mapping`;
    logger.info(`url in createMapping = ${esUrl}`);
    request({
        method: 'POST',
        headers,
        uri: esUrl,
        body: context.faq_docs_mapping,
        json: true,
    }, (err, code, esResult) => {
        if (err || (code || {}).statusCode !== constants.CREATED) {
            return next(`Could not create mapping for type ${context.doc_type}. ${code.statusCode}. err: ${err}. esResult: ${esResult}`);
        }
        logger.info(`Created mapping for type ${context.doc_type}`);
        return next(null, context);
    });
}

function createDocs(context, next) {
    const esUrl = `${ES_DOCS_URL}/${context.doc_type}/`;
    async.eachOfSeries(context.faq_docs, (faqDoc, docIndex, cb) => {
        logger.info(`Creating es doc for faq doc ${JSON.stringify(faqDoc)}`);
        return request({
            method: 'POST',
            headers,
            uri: esUrl,
            body: faqDoc,
            json: true,
            accept: true },
            (err, code, esResult) => {
                if (err || (code || {}).statusCode !== constants.CREATED) {
                    return cb(`Could not create es doc for doc ${JSON.stringify(faqDoc)}. err: ${err}.
                               code: ${code.statusCode}. esResult: ${esResult}`);
                }
                logger.info(`Created the doc with id ${JSON.stringify(esResult)}`);
                return cb(null);
            });
    },
    (err) => {
        if (err) {
            return next(err);
        }
        sleep.sleep(3);
        return next(null, context);
    });
}

function getAllDocs(context, expectedDocsSize, next) {
    const esUrl = `${ES_DOCS_URL}/${context.doc_type}`;
    return request({
        method: 'GET',
        uri: esUrl,
        headers,
        accept: true,
        json: true,
    },
    (err, code, esResult) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Could not get all docs for doc type ${context.doc_type}. ${err}. ${esResult}`);
        }
        context.all_docs = esResult;
        if (context.all_docs.length !== expectedDocsSize) {
            logger.info(`all docs = ${JSON.stringify(context.all_docs, null, 4)}`);
            return next(`Did not get expected no of docs. Expected: ${expectedDocsSize}.
                         Got: ${context.all_docs.length}`);
        }
        logger.info(`Got all docs for doc type ${context.doc_type}`);
        return next(null, context);
    });
}

function getOneDoc(context, next) {
    const randomDocIndex = Math.floor(Math.random() * context.all_docs.length);
    const randomDoc = context.all_docs[randomDocIndex];
    const docId = randomDoc.id;
    const esUrl = `${ES_DOCS_URL}/${context.doc_type}/${docId}`;
    request({
        method: 'GET',
        uri: esUrl,
        headers,
        json: true,
        accept: true,
    }, (err, code, esResult) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Could not get random doc ${docId}.
                         err: ${err}. code: ${code.statusCode}. esResult: ${esResult}`);
        }
        context.random_doc_index = randomDocIndex;
        logger.info(`Got one doc with id ${docId}`);
        return next(null, context);
    });
}

function searchDocs(context, next) {
    const esUrl = `${API_SERVICE_URL}/bots/${TEST_BOT_ID}/es_docs/${context.doc_type}/search`;
    logger.info(`esUrl = ${esUrl}`);
    logger.info(`random index = ${context.random_doc_index}`);
    const question = context.all_docs[context.random_doc_index].question;
    if (!question) {
        logger.info(`Something is wrong. all_docs = ${JSON.stringify(context.all_docs, null, 4)}`);
        logger.info(`question = ${question}`);
        logger.info(`obj = ${JSON.stringify(context.all_docs[context.random_doc_index], null, 4)}`);
        logger.info(`random question = ${context.all_docs[context.random_doc_index].question}`);
        process.exit(1);
    }
    const expectedAnswer = context.all_docs[context.random_doc_index].answer;
    const query = context.query;
    query.query.bool.must[0].match.question = question;
    logger.info(`query = ${JSON.stringify(query, null, 4)}`);
    request({
        method: 'POST',
        uri: esUrl,
        body: query,
        json: true,
        accept: true,
    }, (err, code, esResult) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Search docs failed. question: ${question}.
                         err: ${err}. code: ${code.statusCode}. esResult: ${esResult}`);
        }
        logger.info(`esResult = ${esResult}`);
        const answer = esResult.hits.hits[0]._source.answer;
        if (answer !== expectedAnswer) {
            return next(`Search result did not match: Got: ${answer}. Expected: ${expectedAnswer}`);
        }
        logger.info(`Search API returned the answer I was searching for: ${answer}`);
        return next(null, context);
    });
}

function editOneDoc(context, next) {
    const docId = context.all_docs[context.random_doc_index].id;
    const esUrl = `${ES_DOCS_URL}/${context.doc_type}/${docId}`;
    const newDoc = {
        question: context.all_docs[context.random_doc_index].question,
        answer: '42',
    };
    request({
        method: 'PUT',
        uri: esUrl,
        body: newDoc,
        headers,
        json: true,
        accept: true,
    }, (err, code, esResult) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Could not edit doc ${docId}. err: ${err}.
                         code: ${code.statusCode}. esResult: ${JSON.stringify(esResult)}`);
        }
        logger.info(`Edited the document ${docId} successfully`);
        return next(null, context);
    });
}

function deleteOneDoc(context, next) {
    const docId = context.all_docs[context.random_doc_index].id;
    const esUrl = `${ES_DOCS_URL}/${context.doc_type}/${docId}`;
    logger.info(`esUrl = ${esUrl}`);
    request({
        method: 'DELETE',
        headers,
        uri: esUrl,
        json: true,
    }, (err, code, esResult) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Could not delete document ${docId}. err: ${err}
                         code: ${code.statusCode}. esResult: ${JSON.stringify(esResult)}`);
        }
        logger.info(`Successfully deleted document ${docId}`);
        sleep.sleep(3);
        return next(null, context);
    });
}

function deleteAllDocs(context, next) {
    const esUrl = `${ES_DOCS_URL}/${context.doc_type}`;
    return request({
        method: 'DELETE',
        uri: esUrl,
        headers,
        json: true,
    }, (err, code, esResp) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Could not delete document type ${context.doc_type}.
                        err: ${err}, code: ${code.statusCode}, esResp: ${esResp}`);
        }
        logger.info(`Deleted document type ${context.doc_type}`);
        sleep.sleep(3);
        return next(null, context);
    });
}

function runTest() {
    const testContext = {
        doc_type: 'integration-test',
        faq_docs_file_path: FAQ_DOCS_FILE_PATH,
    };
    async.waterfall([
        function (cb) { deleteAllDocs(testContext, cb); },
        function (context, cb) { readFaqDocsFile(context, cb); },
        function (context, cb) { createMapping(context, cb); },
        function (context, cb) { createDocs(context, cb); },
        function (context, cb) { getAllDocs(context, context.faq_docs.length, cb); },
        function (context, cb) { getOneDoc(context, cb); },
        function (context, cb) { searchDocs(context, cb); },
        function (context, cb) { editOneDoc(context, cb); },
        function (context, cb) { deleteOneDoc(context, cb); },
        function (context, cb) { deleteAllDocs(context, cb); },
        function (context, cb) { getAllDocs(context, 0, cb); },
    ], (err) => {
        if (err) {
            logger.error(`Hit an error during es test with  ${testContext.doc_type}. ${err}`);
            process.exit(1);
        }
        logger.info('Successfully passed es test.');
        process.exit(0);
    });
}
runTest();
