/* eslint-disable no-underscore-dangle */
const async = require('async');
const logger = require('../../common/utils/logger');
const request = require('request');
const constants = require('../../common/constants');

const API_SERVICE_URL = process.env.API_SERVICE_URL;
const TEST_BOT_ID = '3070164780';
const headers = { 'X-Super-Tenant-Api-Key': process.env.SUPER_TENANT_API_KEY };

function getIntentsForBot(botId, next) {
    const intentsUrl = `${API_SERVICE_URL}/bots/${TEST_BOT_ID}/intents`;
    request({
        method: 'GET',
        uri: intentsUrl,
        headers,
        json: true,
        accept: true,
    }, (err, code, intentsResult) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Could not get all intents for bot ${botId}. err: ${err}.
                        code: ${code.statusCode}. intentsResult: ${intentsResult}`);
        }
        const nIntents = intentsResult.length;
        if (nIntents === 0) {
            return next(`Retrieved 0 intents for bot ${botId}. Something is wrong..`);
        }
        logger.info(`Retrieved ${intentsResult.length} intents for bot ${botId}`);
        return next(null);
    });
}

function runTest() {
    async.waterfall([
        function (cb) { getIntentsForBot(TEST_BOT_ID, cb); },
    ], (err) => {
        if (err) {
            logger.error(`Hit an error during intents test. ${err}`);
            process.exit(1);
        }
        logger.info('Successfully passed intents test.');
        process.exit(0);
    });
}

runTest();
