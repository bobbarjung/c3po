/* eslint-disable no-underscore-dangle */
const async = require('async');
const logger = require('../../common/utils/logger');
const request = require('request');
const constants = require('../../common/constants');

const C3PO_SERVICE_URL = process.env.C3PO_SERVICE_URL;
const TEST_BOT_ID = '3070164780';
const TEST_KB_ID = '599f018d45b71c64ee067578';
const headers = { 'X-Super-Tenant-Api-Key': process.env.SUPER_TENANT_API_KEY };

function getKnowledgeBase(kbId, expectedName, next) {
    const kbUrl = `${C3PO_SERVICE_URL}/bots/${TEST_BOT_ID}/knowledge_bases/${kbId}`;
    request({
        method: 'GET',
        uri: kbUrl,
        headers,
        json: true,
        accept: true,
    }, (err, code, kbResult) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Could not read kb doc ${kbId}. err: ${err}.
                         code: ${(code || {}).statusCode}. kbResult: ${kbResult}`);
        }
        const name = kbResult.name;
        if (expectedName && expectedName !== name) {
            return next(`Did not retrieve the correct KB. expected: ${expectedName}. Got: ${name}`);
        } else if (expectedName && expectedName === name) {
            logger.info(`Successfully retrieved the kb doc ${kbId} with expected name: ${name}`);
        } else {
            logger.info(`Successfully retrieved the kb doc ${kbId} with name: ${name}`);
        }
        return next(null);
    });
}

function editKnowledgeBase(kbId, newName, next) {
    const kbUrl = `${C3PO_SERVICE_URL}/bots/${TEST_BOT_ID}/knowledge_bases/${kbId}`;
    const update = {
        name: newName,
    };
    request({
        method: 'PUT',
        uri: kbUrl,
        headers,
        body: update,
        json: true,
        accept: true,
    }, (err, code, kbResult) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Could not edit kb doc ${kbId}. err: ${err}.
                         code: ${code.statusCode}. kbResult: ${kbResult}.`);
        }
        logger.info(`Successfull updated the kb doc ${kbId}`);
        return next(null);
    });
}

function runTest() {
    const newName = `integration-test-${Math.random(1).toFixed(4) * 10000.0}`;
    async.waterfall([
        function (cb) { getKnowledgeBase(TEST_KB_ID, null, cb); },
        function (cb) { editKnowledgeBase(TEST_KB_ID, newName, cb); },
        function (cb) { getKnowledgeBase(TEST_KB_ID, newName, cb); },
    ], (err) => {
        if (err) {
            logger.error(`Hit an error during knowledge_bases test. ${err}`);
            process.exit(1);
        }
        logger.info('Successfully passed knowledge_bases test.');
        process.exit(0);
    });
}

runTest();
