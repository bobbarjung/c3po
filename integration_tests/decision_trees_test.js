/* eslint-disable no-underscore-dangle */
const async = require('async');
const logger = require('../../common/utils/logger');
const request = require('request');
const sleep = require('sleep');
const constants = require('../../common/constants');

const API_SERVICE_URL = process.env.API_SERVICE_URL;
const C3PO_SERVICE_URL = process.env.C3PO_SERVICE_URL;
const TEST_BOT_ID = '3070164780';
const TEST_INTENT_ID = '3070164780_dtree-test';
const dataFile1 = '3070164780/uploads/private/decision_trees/3070164780/decision_trees/levis1.csv';
const dataFile2 = '3070164780/uploads/private/decision_trees/3070164780/decision_trees/levis2.csv';
const dtreeUrl = `${C3PO_SERVICE_URL}/bots/${TEST_BOT_ID}/intents/${TEST_INTENT_ID}/decision_trees`;
const headers = { 'X-Super-Tenant-Api-Key': process.env.SUPER_TENANT_API_KEY };

const editedAttributes = {
    attributes: [
        {
            attribute: 'gender',
            question: 'what is the gender',
            enabled: 'true',
        },
        {
            attribute: 'fit_type',
            question: 'what is the fit type',
            enabled: true,
        },
        {
            attribute: 'leg_opening',
            question: 'what is the leg opening',
            enabled: true,
        },
        {
            attribute: 'rise_type',
            question: 'how do you like the rise',
            enabled: true,
        },
        {
            attribute: 'stretch',
            question: 'do you want stretch in your jeans',
            enabled: true,
        },
        {
            attribute: 'product_type',
            question: 'Chose from these product types',
            enabled: true,
        },
        {
            attribute: 'subtitle',
            enabled: true,
        },
        {
            attribute: 'image_url',
            enabled: true,
        },
    ],
};

const queries = {
    gender: [
        {
            where: {},
            select: ['gender'],
            uniq: 'gender',
        },
        ['Male', 'Female'].sort(),
    ],
    male_fit_type: [
        {
            where: {
                gender: 'Male',
            },
            select: ['fit_type'],
            uniq: 'fit_type',
        },
        ['Tight', 'Fitted', 'Regular', 'Roomy', 'Loose'].sort(),
    ],
    female_tight_high_rise_no_stretch_product_type: [
        {
            where: {
                gender: 'Female',
                fit_type: 'Tight',
                rise_type: 'High',
                stretch: 'No',
            },
            select: ['product_type'],
            uniq: 'product_type',
        },
        ['501 Skinny'],
    ],
    female_fitted_rise_type: [
        {
            where: {
                gender: 'Female',
                fit_type: 'Fitted',
            },
            select: ['rise_type'],
            uniq: 'rise_type',
        },
        ['Low', 'Medium', 'High'].sort(),
    ],
    male_loose_wide: [
        {
            where: {
                gender: 'Male',
                fit_type: 'Loose',
                leg_opening: 'Wide',
            },
            select: ['product_type'],
            uniq: 'product_type',
        },
        ['559 Relaxed Straight', '569 Loose Straight Jeans'].sort(),
    ],
    male_loose_narrow: [
        {
            where: {
                gender: 'Male',
                fit_type: 'Loose',
                leg_opening: 'Narrow',
            },
            select: ['product_type'],
            uniq: 'product_type',
        },
        [],
    ],
};

const retryQuery = [
    {
        where: {
            gender: 'Male',
            fit_type: 'Loose',
            leg_opening: 'Narrow',
        },
        select: ['product_type'],
        uniq: 'product_type',
    },
    ['589 Loose Narrow Jeans'],
];


function createDtree(dataFilePath, next) {
    const requestBody = {
        name: 'test-dtree',
        description: 'test-dtree-description',
        data_file_path: dataFilePath,
    };
    return request({
        method: 'POST',
        headers,
        uri: dtreeUrl,
        body: requestBody,
        json: true,
        accept: true },
        (err, code, dtreeDoc) => {
            if (err || (code || {}).statusCode !== constants.OK) {
                return next(`Could not create dtree. ${err}`);
            }
            logger.info(`Created dtree ${dtreeDoc._id}`);
            return next(null, dtreeDoc._id);
        });
}

function getDtree(dtreeId, next) {
    request({
        method: 'GET',
        headers,
        uri: `${dtreeUrl}/${dtreeId}`,
    }, (err, code, body) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Could not retrieve dtree. ${err}`);
        }
        const dtreeDoc = JSON.parse(body);
        logger.info(`dtreeDoc = ${JSON.stringify(dtreeDoc)}`);
        if (dtreeDoc.status === 'active') {
            logger.info('The created tree is active');
            return next(null, dtreeId);
        }
        logger.info(`Created dtree is in ${dtreeDoc.status} state`);
        sleep.sleep(5);
        return getDtree(dtreeId, next);
    });
}

function deleteDtree(dtreeId, next) {
    logger.info(`Deleting dtree ${dtreeId}`);
    request({
        method: 'DELETE',
        headers,
        uri: `${dtreeUrl}/${dtreeId}`,
    }, (err, code) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Could not delete dtree ${dtreeId}. Reason: ${err}`);
        }
        return next(null);
    });
}

function editDtreeAttributes(dtreeId, next) {
    logger.info(`Editing attributes for dtree ${dtreeId}`);
    request({
        method: 'PUT',
        headers,
        uri: `${dtreeUrl}/${dtreeId}`,
        json: true,
        body: editedAttributes,
    }, (err, code, dtreeDoc) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Could not edit dtree ${dtreeId}. Reason: ${err}`);
        }
        logger.info(`Edited dtree: ${dtreeDoc}`);
        return next(null, dtreeId);
    });
}

function searchDtree(dtreeId, next) {
    // For each of the query compare the response to the expected response.
    const apiUrl = `${API_SERVICE_URL}/bots/${TEST_BOT_ID}/intents/${TEST_INTENT_ID}/decision_trees`;
    const url = `${apiUrl}/${dtreeId}/search_records`;
    logger.info(`Query url: ${url}`);
    async.eachOfSeries(Object.keys(queries), (queryKey, qIndex, cb) => {
        request({
            method: 'POST',
            uri: url,
            json: true,
            headers,
            body: queries[queryKey][0],
        }, (err, code, queryResult) => {
            if (err || (code || {}).statusCode !== 200) {
                return cb(`Failure to search dtree ${dtreeId} with query: ${JSON.stringify(queryKey)}: ${err}. Code: ${code.statusCode}`);
            }
            logger.info(`query = ${JSON.stringify(queries[queryKey])}`);
            logger.info(`Result of search query = ${JSON.stringify(queryResult)}`);
            const uniqs = Object.keys(queryResult).sort();
            if (JSON.stringify(uniqs) !== JSON.stringify(queries[queryKey][1])) {
                return cb(`Unexpected response. Query: ${JSON.stringify(queryKey)}.
                           Expected: ${JSON.stringify(queries[qIndex][1])},
                           Got: ${JSON.stringify(uniqs)}`);
            }
            return cb(null);
        });
    }, err => next(err, dtreeId));
}

function editDtreeDataFile(dtreeId, dataFile, next) {
    logger.info(`Editing datafile for dtree ${dtreeId} with ${dataFile}`);
    request({
        method: 'PUT',
        headers,
        uri: `${dtreeUrl}/${dtreeId}`,
        json: true,
        body: { data_file_path: dataFile },
    }, (err, code, dtreeDoc) => {
        if (err || (code || {}).statusCode !== constants.OK) {
            return next(`Could not edit datafile of dtree ${dtreeId}. Reason: ${err}`);
        }
        logger.info(`Edited dtree: ${dtreeDoc}`);
        return next(null, dtreeId);
    });
}

function runTest() {
    // DTREE APIs
    // Create Dtree
    // Get Dtree
    // Edit Dtree attributes
    // Search Dtree
    // Edit Dtree data file
    // Search Dtree
    // Get Dtree
    // Delete Dtree
    async.waterfall([
        function (cb) { createDtree(dataFile1, cb); },
        function (dtreeId, cb) { getDtree(dtreeId, cb); },
        function (dtreeId, cb) { editDtreeAttributes(dtreeId, cb); },
        function (dtreeId, cb) { searchDtree(dtreeId, cb); },
        function (dtreeId, cb) { editDtreeDataFile(dtreeId, dataFile2, cb); },
        function (dtreeId, cb) { getDtree(dtreeId, cb); },
        function (dtreeId, cb) {
            queries.male_loose_narrow = retryQuery;
            searchDtree(dtreeId, cb);
        },
        function (dtreeId, cb) { deleteDtree(dtreeId, cb); },
    ], (err, dtreeId) => {
        if (err) {
            logger.error(`Hit an error during test with dtree ${dtreeId}. ${err}`);
            process.exit(1);
        }
        logger.info('Successfully passed dtree test.');
        process.exit(0);
    });
}

runTest();
