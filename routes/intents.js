/* eslint-disable no-underscore-dangle */
const express = require('express');
const Intent = require('../../common/models/intent');
const constants = require('../../common/constants');
const logger = require('../../common/utils/logger');
const RouteHelper = require('./route_helper');
const DTrees = require('./decision_trees');

const router = express.Router();

router.get('/', (req, res) => {
    const botId = req.bot._id;
    logger.info(`Retrieving intents for bot: ${botId}`);
    Intent.getByBotId(botId, (err, intents) => {
        if (err) {
            logger.error(`Could not retrieve intents for bot: ${botId}. Reason: ${err}`);
            return res.status(constants.RUNTIME_ERROR).send(err);
        }
        return res.status(constants.OK).send(intents);
    });
});

router.use('/:intent_id/decision_trees', RouteHelper.validateIntentIdInBot, DTrees);

module.exports = router;
