const express = require('express');
const RouteHelper = require('./route_helper');
const DTreeHelper = require('../utils/decision_tree_helper');

const router = express.Router();

// Get current contents of this intent's decision tree.
// For now assume that there is at most one decision tree
// per intent. When this assumption changes, we can return an array here.
router.get('/',
           DTreeHelper.getDecisionTree);

// Get current contents of decision tree
router.get('/:tree_id',
           RouteHelper.validateTreeIdInIntent, DTreeHelper.getDecisionTree);

// Create a decision tree
router.post('',
            DTreeHelper.createDecisionTree);

// Update a decision tree
router.put('/:tree_id',
           RouteHelper.validateTreeIdInIntent,
           DTreeHelper.updateDecisionTree);

router.delete('/:tree_id',
              RouteHelper.validateTreeIdInIntent,
              DTreeHelper.deleteDecisionTree);

module.exports = router;
