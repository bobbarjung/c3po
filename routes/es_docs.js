const express = require('express');
const constants = require('../../common/constants');
const logger = require('../../common/utils/logger');
const RouteHelper = require('./route_helper');
const EsWrapper = require('../utils/es_wrapper');

const router = express.Router();

// Create a doc type with mapping
router.post('/:doc_type/mapping',
            EsWrapper.checkAndCreateIndexAndDocTypeForBot);

// Create a doc
router.post('/:doc_type/',
            EsWrapper.createDocForBot);

// Get all docs of a particular type
router.get('/:doc_type',
           EsWrapper.getAllDocsForBot);

// Get a particular document
router.get('/:doc_type/:doc_id',
           EsWrapper.getDocForBot);

router.put('/:doc_type/:doc_id',
           EsWrapper.editDocForBot);

// Delete all docs of a particular doc type.
router.delete('/:doc_type',
              EsWrapper.deleteDocTypeForBot);

// Delete a ES doc of a particular type
router.delete('/:doc_type/:doc_id',
              RouteHelper.superTenantAuthenticate,
              EsWrapper.deleteDocForBot);

module.exports = router;
