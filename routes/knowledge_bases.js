const express = require('express');
const KnowledgeBaseDB = require('../../common/models/knowledge_base');

const router = express.Router();

// Create a Knowledge base
// TODO: validate that body has a bot_id param
router.post('/',
            KnowledgeBaseDB.handleCreate);

// Update Knowledge base settings
// TODO: validate that the knowledge base id is part of bot bot_id before editing.
router.put('/:id',
           KnowledgeBaseDB.handleUpdate);

// Get Knowledgebase details
// TODO: validate that knowledge_base id is part of the bot_id before returning
router.get('/:id',

           KnowledgeBaseDB.handleFetch);

// TODO: Add a delete route.
// TODO: write an integratio test that exercises all the routes in this file.

module.exports = router;
