const express = require('express');
const Bot = require('../../common/models/bot_config');
const constants = require('../../common/constants');
const logger = require('../../common/utils/logger');
const RouteHelper = require('./route_helper');
const EsWrapper = require('../utils/es_wrapper');
const intents = require('./intents');
const EsDocs = require('./es_docs');
const KnowledgeBases = require('./knowledge_bases');

const router = express.Router();

router.use('/:bot_id/intents', RouteHelper.superTenantAuthenticate, RouteHelper.validateBotId, intents);
router.use('/:bot_id/knowledge_bases', RouteHelper.superTenantAuthenticate, RouteHelper.validateBotId, KnowledgeBases);

router.use('/:bot_id/es_docs', RouteHelper.superTenantAuthenticate, RouteHelper.validateBotId, EsDocs);

router.get('/:bot_id/',
           RouteHelper.superTenantAuthenticate,
           // We expect that by now the bot document will be cached in req.bot
           RouteHelper.cacheBot, (req, res) => res.status(constants.OK).send(req.bot));


// Returns All active bots.
// If a query paramter all_bots=true is set, then this returns *all* bots
// including deleted bots.
router.get('/', RouteHelper.superTenantAuthenticate, (req, res) => {
    logger.info(`Here in bots...`)
    let botFilter = { bot_status: 'active' };
    if (req.query.all_bots && req.query.all_bots === 'true') {
        botFilter = {};
    }
    Bot.find(botFilter, { _id: true, bot_name: true }, ((err, bots) => {
        if (err) {
            return res.status(constants.RUNTIME_ERROR).send(err);
        }
        return res.status(constants.OK).send(bots);
    }));
});

module.exports = router;
