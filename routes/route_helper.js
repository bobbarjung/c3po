const RouteHelper = module.exports;
const Bot = require('../../common/models/bot_config');
const constants = require('../../common/constants');
const Intent = require('../../common/models/intent');
const logger = require('../../common/utils/logger');

RouteHelper.superTenantAuthenticate = function (req, res, next) {
    // Check for supertenant API Key
    if (req.get('X-Super-Tenant-Api-Key') === process.env.SUPER_TENANT_API_KEY) {
        return next();
    }
    return res.status(400).send('Access Denied');
};

RouteHelper.notifierAuthenticate = function (req, res, next) {
    // TODO: If caller is a valid notifier, then authenticate
    return next();
};

// Vaidate that a given id is a valid bot_id.
// Also cache the bot_cofig in req.bot
RouteHelper.validateBotId = function (req, res, next) {
    const projection = {_id: true, bot_name: true}
    Bot.findById(req.params.bot_id, projection, (err, botDoc) => {
        if (err || !botDoc) {
            return res.status(constants.NOT_FOUND).send(`Not a valid bot id: ${req.params.bot_id}`);
        }
        req.bot = botDoc; // Cache the bot document for re-use
                      // as we pass the req object through the handlers
        return next();
    });
};

RouteHelper.cacheBot = function(req, res, next) {
    Bot.findById(req.params.bot_id, {}, (err, botDoc) => {
        if (err || !botDoc) {
            return res.status(constants.NOT_FOUND).send(`Not a valid bot id: ${req.params.bot_id}`);
        }
        req.bot = botDoc; // Cache the bot document for re-use
                      // as we pass the req object through the handlers
        return next();
    });
}

// Validate that a given intent_id is in the given bot_id
// Also cache the intent_id in req.intent_id
RouteHelper.validateIntentIdInBot = function (req, res, next) {
    const botId = req.params.bot_id || req.bot._id;
    if (!req.params.intent_id) {
        return res.status(constants.BAD_REQUEST).send('intent_id must be specified');
    }
    if (!botId) {
        return res.status(constants.BAD_REQUEST).send('bot_id must be specified');
    }
    Intent.getByBotId(botId, function(err, intents) {
        if(err) {
            logger.error("Could not retrieve intents for bot: ", botId, ". Reason: ", err);
            return res.status(constants.RUNTIME_ERROR).send(err);
        }
        const intent_ids = intents.map(function(intent) {return intent._id});
        if (!intent_ids.includes(req.params.intent_id)) {
            return res.status(constants.BAD_REQUEST).send(`${req.params.intent_id} is not an intent in the specified bot`);
        }
        req.intent_id = req.params.intent_id; // Cache the intent_id
        return next();
    })
}

// Validate that a given tree_id is in a given intent_id
// Also cache the intent doc in req.intent
RouteHelper.validateTreeIdInIntent = function (req, res, next) {
    const intentId = req.params.intent_id || req.intent_id;
    Intent.findOne({_id: intentId}, function(err, intent_doc) {
        if (err) {
            logger.error(`Could not look up intent ${intentId} In mongo. ${err}`);
            return res.status(constants.RUNTIME_ERROR).send("Failed to lookup intent");
        }
        if (!(intent_doc.decision_trees || []).includes(req.params.tree_id)) {
            return res.status(constants.BAD_REQUEST).send(`Tree Id ${req.params.tree_id} is not a member of the specified intent`);
        }
        req.intent = intent_doc; // Cache the intent.
        return next();
    })
}

module.exports = RouteHelper;
