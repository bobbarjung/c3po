var express         = require('express');
var router          = express.Router();
var logger          = require('../../common/utils/logger');
var AnalyticsHelper = require('../utils/analytics_helper');
var RouteHelper     = require('../routes/route_helper');


// API takes the following query params
// start_time in iso8601 - defaults to start of today in local time
// end_time in is8601 - defaults to now.
// utc_offset in minutes - For example, PST offset will be +420, defaults to UTC/0
// intent_id = defaults to all intents
router.get('/bots/:bot_id/chat_messages',
           RouteHelper.superTenantAuthenticate,
           AnalyticsHelper.getChatMessages);

// API takes the following query params
// start_time in iso8601 - defaults to start of today in local time
// end_time in is8601 - defaults to now.
// utc_offset in minutes - For example, PST offset will be +420, defaults to UTC/0
// Returns user information for the bot
router.get('/bots/:bot_id/users',
           RouteHelper.superTenantAuthenticate,
           AnalyticsHelper.getUserInfo);


// API takes the following query params
// start_time in iso8601 - defaults to start of today in local time
// end_time in is8601 - defaults to now.
// utc_offset in minutes - For example, PST offset will be +420, defaults to UTC/0
// Returns intent information for the bot.
router.get('/bots/:bot_id/intents',
           RouteHelper.superTenantAuthenticate,
           AnalyticsHelper.getIntentInfo);

// API take the following query params
// start_time in iso8601 - defaults to start of today in local time
// end_time in is8601 - defaults to now.
// utc_offset in minutes - For example, PST offset will be +420, defaults to UTC/0
// intents - comma seperated list of intent_ids.
router.get('/bots/:bot_id/unique_users_for_intents',
            RouteHelper.superTenantAuthenticate,
            AnalyticsHelper.getUniqueUsersForIntents);

module.exports = router;